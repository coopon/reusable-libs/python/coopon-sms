# currently msg91 and 2factor services are supported
# in-order to use one of the service, uncomment any
# one of the import statement below

from msg91_otp.client import OTPClient
#from twoFactor_otp.client import OTPClient

from coopon_sms.sms_response import ServiceResponse

class SMSClient:
    """library agnostic sms client"""

    def __init__(self, auth_token):
        """constructor for SMSClient
        Args:
            auth_token (str): authorization token for the client
        """
        self.auth_token = auth_token
        # library specific client
        self._otp_client = OTPClient(auth_token)

    def send_otp(self, receiver, **kwargs):
        """sends OTP to the receiver
        Args:
            receiver (str): mobile number of the receipient
            otp (int, optional): custom OTP value 4 to 6 digits

            MSG91 only:
                message (str, optional): custom message along with OTP
                sender (str, optional): how should your name appear on SMS
                otp_length (int, optional): length of the OTP value

            2FACTOR only:
                template_name (str, optional): custom template name for message in server
        Returns:
            response object with status and message
        """
        response = self._otp_client.send_otp(receiver, **kwargs)
        identifier = None
        if hasattr(response, "identifier"):
            identifier = response.identifier
        return ServiceResponse(response.status, response.message, identifier=identifier)

    def resend_otp(self, receiver):
        """resends OTP message to the receiver
        Args:
            receiver (str): mobile number of the receipient
        Returns:
            response object with status and message
        """
        response = self._otp_client.resend_otp(receiver)
        identifier = None
        if hasattr(response, "identifier"):
            identifier = response.identifier
        return ServiceResponse(response.status, response.message, identifier=identifier)

    def verify_otp(self, identifier, otp_value):
        """verifies a OTP value against identifier with SMS service
        Args:
            identifier (str): depends on the service. Some service
            uses message_id, some uses mobile number
            opt_value (str): x digit value representing OTP
        Returns:
            response object with verification status and message
        """
        response = self._otp_client.verify_otp(identifier, otp_value)
        return ServiceResponse(response.status, response.message)

    def __repr__(self):
        return f"<{self.__class__.__name__}>"
