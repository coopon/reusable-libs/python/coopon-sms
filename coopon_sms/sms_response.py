class ServiceResponse:
    def __init__(self, status, message, **kwargs):
        self.status = status
        self.message = message

        if 'identifier' in kwargs:
            identifier = kwargs.get('identifier')
            if identifier:
                self.identifier = identifier

    def __repr__(self):
        return f"<Response: {self.status}, {self.message}>"
