import re

# 10 digit mobile number pattern
VALID_MOBILE_PATTERN_INDIA = re.compile(r"^(\+)?(91)?(\d{10})+$")

def clean_mobile_number(number: str):
    """Removes unwanted elements from phone number
    Arguments:
        number: the mobile number to be cleaned as string
    Returns:
        input string without '+91', '-' and whitespaces
    """
    number = number.strip() \
                   .replace('+91', '') \
                   .replace('-', '') \
                   .replace(' ', '')
    return number


def is_valid_mobile_number(number, pattern=VALID_MOBILE_PATTERN_INDIA):
    """Checks if a given number matches the supplied pattern.
    Arguments:
        number: the mobile number to be validated as string
        pattern: the re.Pattern to match. default is VALID_MOBILE_PATTERN_INDIA
    Returns:
        is_valid: Boolean representing valid or not
    """
    # assume all number to be invalid
    is_valid = False

    match_found = pattern.match(number)
    if match_found:
        is_valid = True
    
    return is_valid
