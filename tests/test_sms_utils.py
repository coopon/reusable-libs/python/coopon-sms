from coopon_sms.utils import clean_mobile_number, is_valid_mobile_number

valid_numbers = [
            "9876543210", "+919876543210", "+91-9876543210",
            "987 654 3210", "987-654-3210"]

invalid_numbers = ["a9876543210", "12123", "qasdfsadf", "+91-9343#4232"]

def test_clean_mobile_number():
    for number in valid_numbers:
        output = clean_mobile_number(number)
        assert "+91" not in output
        assert "-" not in output
        assert " "  not in output

def test_validate_mobile_number():
    for number in valid_numbers:
        number = clean_mobile_number(number)
        assert True == is_valid_mobile_number(number)

    for number in invalid_numbers:
        number = clean_mobile_number(number)
        assert False == is_valid_mobile_number(number)

