import setuptools
from setuptools import find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="coopon-sms",
    version="0.1.0",
    author="Prasanna Venkadesh",
    author_email="prasanna@cooponscitech.in",
    description="Reusable SMS Library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires='>=2.7',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    url="",
    install_requires=["msg91-otp==0.0.2", "2factor-otp==0.0.1"],
    classifiers=[
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: CPython',

        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
    ]
)

